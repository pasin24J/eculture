import React from "react";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Chat from "@material-ui/icons/Chat";
import Build from "@material-ui/icons/Build";
// core components
import Button from "components/CustomButtons/Button.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomTabs from "components/CustomTabs/CustomTabs.jsx";
import tabsStyle from "assets/jss/material-kit-react/views/componentsSections/tabsStyle.jsx";

class SectionTabs extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <div className={classes.container}>
          <div id="nav-tabs">
            <h3>ฝึกโขนขั้นพื้นฐาน</h3>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <h3 className={classes.title}>
                  <small>การตั้งวง</small>
                </h3>
                <CustomTabs
                  headerColor="primary"
                  tabs={[
                    {
                      tabName: "การตั้งวง",
                      tabIcon: Face,
                      tabContent: (
                        <div style={{position: 'relative', width: '100%', height:'auto', textAlign: 'center'}}>
                        <img src="http://www.manager.co.th/asp-bin/Image.aspx?ID=1602035" width= '95%' height ='auto' />
                        <div  id="buttons" className={classes.buttonCard}>
                          <Button href="../../camera.html" color="actbutton" size="sm">เริ่มฝึก</Button>
                        </div>
                        </div>
                      )
                    }
                  ]}
                />
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(tabsStyle)(SectionTabs);
