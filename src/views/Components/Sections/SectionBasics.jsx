import React from "react";
// react plugin that creates slider
import Nouislider from "react-nouislider";
import axios from "axios";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Radio from "@material-ui/core/Radio";
import Switch from "@material-ui/core/Switch";
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";
import People from "@material-ui/icons/People";
import Check from "@material-ui/icons/Check";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import CustomLinearProgress from "components/CustomLinearProgress/CustomLinearProgress.jsx";
import Paginations from "components/Pagination/Pagination.jsx";
import Badge from "components/Badge/Badge.jsx";
import image from "assets/img/faces/avatar.jpg";

import {getAllCustomers} from '../../../Firestore/database'

import basicsStyle from "assets/jss/material-kit-react/views/componentsSections/basicsStyle.jsx";

class SectionBasics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: [24, 22],
      selectedEnabled: "b",
      checkedA: true,
      checkedB: false,
      topicSugguest: []
    };
    this.handleChangeEnabled = this.handleChangeEnabled.bind(this);
  }
  componentDidMount = async () => {
      this.getData();
  }
  getData = async () => {
    let virList = ["vir_225_1533289131","vir_228","vir_224","vir_229","vir_232","vir_231"];
    let history = {};

    let test = await getAllCustomers();
    console.log("test",test)
    const cachedHits = localStorage.getItem("searchHistory");
    if (cachedHits) {
      history = JSON.parse(cachedHits);
      return;
    }

    let province = (history.province && `&property=col_8&operator=CONTAINS&valueLiteral=${history.province}`) || "";
    let category = (history.category && `&property=col_9&operator=S_EQUALS&valueLiteral=${history.category}`) || "";
    let relateWord = (history.relateWord && `&property=col_5&operator=CONTAINS&valueLiteral=${history.relateWord}`) || "";
    if (province === "" && category === "" && relateWord === "") {
      virList = ["vir_225_1533289131"];
    }
    let set = await Promise.all(virList.map(async (vir) => {
      let newFeed = await axios.get(`https://api.data.go.th/search_virtuoso/api/dataset/query?dsname=${vir}${province}${category}${relateWord}&loadAll=1&type=json&limit=4&offset=0`);
      console.log(`https://api.data.go.th/search_virtuoso/api/dataset/query?dsname=${vir}${province}${category}${relateWord}&loadAll=1&type=json&limit=4&offset=0`)
      if (newFeed && newFeed.data && newFeed.data.data && newFeed.data.data[0].title !== "-") {
        console.log("old:",newFeed)
        return newFeed.data.data;
      }
    }));
    set = set.filter(function( element ) {
      return element !== undefined;
   });
   set = set.reduce((old,data) => {
     old = [...old,...data]
     return old
   },[])
    this.setState({
      topicSugguest: set
    });
    console.log(set);
  }
  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };
  handleChangeEnabled(event) {
    this.setState({ selectedEnabled: event.target.value });
  }
  handleToggle(value) {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked
    });
  }
  renderTopicCard = () => {
    let {topicSugguest} = this.state;
    const { classes } = this.props;
    return ( topicSugguest.map((data,index) => {
      return (
        <div id="buttons" key={index} style={{marginTop: "15px", marginBottom: "15px"}}>
          <div className= {classes.imgRaised + " " + classes.imgRounded + " " + classes.imgFluid}>
            <img
              src={image}
              alt="..."
              className={classes.imgRounded + " " + classes.imgFluid + " " + classes.imgCard}
            />
            <div className={classes.topicCard}>
              <h4>{data.title}</h4>
            </div>
            <div className={classes.descCard}>
              {data.description.split("&lt;").join("<").split("&gt;").join(">").replace(/(<([^>]+)>)/ig,"")}
            </div>
            <div className={classes.buttonCard}>
              <Button color="info" size="sm"> ศึกษาเพิ่มเติม.. </Button>
            </div>
          </div>
        </div>
      )
    })
    )
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.sections}>
        <div className={classes.container}>
          <div className={classes.title}>
            <h2>แนะนำหัวข้อ</h2>
          </div>
          {this.renderTopicCard()}
          <hr />
          <div className={classes.title}>
            <h2>แนะนำกิจกรรม</h2>
          </div>
          {this.renderTopicCard()}
        </div>
      </div>
    );
  }
}
export default withStyles(basicsStyle)(SectionBasics);
