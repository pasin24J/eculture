import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";
import Button from "components/CustomButtons/Button.jsx";

import timelineStyle from "assets/jss/material-kit-react/views/landingPageSections/timelineStyle.jsx";

import {Timeline, TimelineEvent} from 'react-event-timeline'
class timelineSection extends React.Component {
  render() {
    const { classes } = this.props;
    return (<div className={classes.section}>
              <Timeline lineStyle={{background: "#63223c"}}>
                      <TimelineEvent title="ก่อตั้งอาณาจักรอยุธยา"
                                      subtitle="พ.ศ. 1893"
                                      icon={<i className="material-icons md-18">textsms</i>} 
                                      titleStyle={{color: "#a5494d", fontSize:"16px"}} 
                                      contentStyle={{backgroundColor: "#ecd292", color: "#d27254"}}
                                      subtitleStyle={{color: "#e5a267", fontSize:"14px"}}
                                      bubbleStyle={{backgroundColor: "#e5a267"}}
                                      iconColor="#63223c"
                                      
                      >   
                              <p className={classes.descCard}>
                              อาณาจักรอยุธยาเริ่มก่อตั้งขึ้นเมื่อปี พ.ศ. 1893 โดยเวลานั้นพระเจ้าอู่ทองได้ทำการสถาปนากรุงศรีอยุธยาขึ้นมา
                              </p>
                              
                                  <div id="buttons" className={classes.buttonCard}>
                                    <Button href="landing-page-srisunpet" color="timelinebutton" size="sm"> ศึกษาเพิ่มเติม.. </Button>
                                  </div>
                      </TimelineEvent>
                      {/* Image */}
                        <div style={{position: 'relative', margin: '10px 0px', paddingLeft: '45px', textAlign: 'left'}}>
                        <img src="http://1.bp.blogspot.com/-1f39rbpMx20/VImKJ-WO2ZI/AAAAAAAAABo/MMhpi-FWrS8/s1600/1.2.jpg" width= '95%' height ='auto' />
                        </div>
                        
                        <TimelineEvent title="การเสียกรุงครั้งที่ 2"
                                      subtitle="พ.ศ.2310"
                                      icon={<i className="material-icons md-18">textsms</i>} 
                                      titleStyle={{color: "#a5494d", fontSize:"16px"}} 
                                      contentStyle={{backgroundColor: "#ecd292", color: "#d27254"}}
                                      subtitleStyle={{color: "#e5a267", fontSize:"14px"}}
                                      bubbleStyle={{backgroundColor: "#e5a267"}}
                                      iconColor="#63223c"
                                      
                      >   
                              <p className={classes.descCard}>
                              การที่อยุธยาขยายอำนาจไปทางภาคเหนือ ทำให้เกิดการจลาจลแย่งชิงอำนาจกันระหว่าง เมืองเชียงใหม่และอาณาจักรมอญ 
                              </p>
                              
                                  <div id="buttons" className={classes.buttonCard}>
                                    <Button color="timelinebutton" size="sm"> ศึกษาเพิ่มเติม.. </Button>
                                  </div>
                      </TimelineEvent>
                      <div style={{position: 'relative', margin: '10px 0px', paddingLeft: '45px', textAlign: 'left'}}>
                        <img src="https://i.ytimg.com/vi/P0dPmwvywLA/maxresdefault.jpg" width= '95%' height ='auto' />
                        </div>

                      <TimelineEvent title="ขึ้นทะเบียนเป็นแหล่งมรดกโลก"
                                      subtitle="พ.ศ. 2534 "
                                      icon={<i className="material-icons md-18">textsms</i>} 
                                      titleStyle={{color: "#a5494d", fontSize:"16px"}} 
                                      contentStyle={{backgroundColor: "#ecd292", color: "#d27254"}}
                                      subtitleStyle={{color: "#e5a267", fontSize:"14px"}}
                                      bubbleStyle={{backgroundColor: "#e5a267"}}
                                      iconColor="#63223c"
                                      
                      >   
                              <p className={classes.descCard}>
                              ในสมัยของ จอมพล ป. พิบูลสงคราม ได้มีการเริ่มโครงการบูรณะพระที่นั่ง และวัดต่างๆ โดยมีกรมศิลปากรเป็นผู้ดูแล 
                              </p>
                              
                                  <div id="buttons" className={classes.buttonCard}>
                                    <Button color="timelinebutton" size="sm"> ศึกษาเพิ่มเติม.. </Button>
                                  </div>
                              
                            
                      </TimelineEvent>
                        
              </Timeline>,
      </div>
    );
  }
}

export default withStyles(timelineStyle)(timelineSection);
