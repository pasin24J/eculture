import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";

import productStyle from "assets/jss/material-kit-react/views/landingPageSections/productStyle.jsx";

class ProductSection extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={8}>
            <h5 className={classes.descCard}>
            อาณาจักรสุโขทัยตั้งอยู่บนที่ราบลุ่มแม่น้ำยม สถาปนาขึ้นราวพุทธศตวรรษที่ 18 ในฐานะสถานีการค้าของรัฐละโว้ หลังจากนั้นราวปี 1800 พ่อขุนบางกลางหาวและพ่อขุนผาเมือง ได้ร่วมกันกระทำการยึดอำนาจจากขอมสบาดโขลญลำพง ซึ่งทำการเป็นผลสำเร็จและได้สถาปนาเอกราชให้รัฐสุโขทัยเป็นอาณาจักรสุโขทัยและมีความเจริญรุ่งเรืองตามลำดับและเพิ่มถึงขีดสุดในสมัยพ่อขุนรามคำแหงมหาราช ก่อนจะค่อย ๆ ตกต่ำ และประสบปัญหาทั้งจากปัญหาภายนอกและภายใน จนต่อมาถูกรวมเป็นส่วนหนึ่งของอาณาจักรอยุธยาไปในที่สุด
            </h5>
          </GridItem>
        </GridContainer>
        <div>
        <div style={{position: 'relative', margin: '10px 0px', textAlign: 'center'}}>
                <img src="https://image.dek-d.com/25/2910745/113343416" width= '95%' height ='auto' />
                </div>
        </div>
      </div>
    );
  }
}

export default withStyles(productStyle)(ProductSection);
