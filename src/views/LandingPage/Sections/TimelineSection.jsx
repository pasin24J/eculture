import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";
import Button from "components/CustomButtons/Button.jsx";

import productStyle from "assets/jss/material-kit-react/views/landingPageSections/timelineStyle.jsx";

import {Timeline, TimelineEvent} from 'react-event-timeline'

class ProductSection extends React.Component {
  render() {
    const { classes } = this.props;
    return (<div className={classes.section}>
        
              <Timeline lineStyle={{background: "	#63223c"}}>
                      <TimelineEvent title="สถาปนาอาณาจักรสุโขทัย"
                                    subtitle="พุทธศตวรรษที่ 18 "
                                    icon={<i className="material-icons md-18">textsms</i>
                                 }  titleStyle={{color: "#a5494d", fontSize:"16px"}} 
                                 contentStyle={{backgroundColor: "#ecd292", color: "#d27254"}}
                                 subtitleStyle={{color: "#e5a267", fontSize:"14px"}}
                                 bubbleStyle={{backgroundColor: "#e5a267"}}
                                 iconColor="#63223c"
                                 
                      >
                      <p className={classes.descCard}>
                          อาณาจักรสุโขทัยตั้งอยู่บนที่ราบลุ่มแม่น้ำยม สถาปนาขึ้นราวพุทธศตวรรษที่ 18 ในฐานะสถานีการค้าของรัฐละโว้ 
                          </p>

                          <div id="buttons" className={classes.buttonCard}>
                                    <Button href="landing-page" color="timelinebutton" size="sm"> ศึกษาเพิ่มเติม.. </Button>
                                  </div>

                      </TimelineEvent>
                        <div style={{position: 'relative', margin: '10px 0px', paddingLeft: '45px', textAlign: 'left'}}>
                        <img src="https://pck24-09.weebly.com/uploads/1/8/8/3/18830350/173818.jpg" width= '95%' height ='100%' />
                        </div>
                        <TimelineEvent title="ความเสื่อมของอาณาจักรสุโขทัย"
                                    subtitle="พ.ศ. 1841"
                                    icon={<i className="material-icons md-18">textsms</i>
                                 }  titleStyle={{color: "#a5494d", fontSize:"16px"}} 
                                 contentStyle={{backgroundColor: "#ecd292", color: "#d27254"}}
                                 subtitleStyle={{color: "#e5a267", fontSize:"14px"}}
                                 bubbleStyle={{backgroundColor: "#e5a267"}}
                                 iconColor="#63223c"
                                 
                      >
                      <p className={classes.descCard}>
                      ความเสื่อมของอาณาจักรสุโขทัยเกิดขั้นเพราะความอ่อนแอของระบบการเมืองการปกครอง  แคว้นต่างๆ  ที่เคยอยู่ในอำนาจตั้งตนเป็นอิสระ
                          </p>

                          <div id="buttons" className={classes.buttonCard}>
                                    <Button href="" color="timelinebutton" size="sm"> ศึกษาเพิ่มเติม.. </Button>
                                  </div>

                      </TimelineEvent>
                      
                      <TimelineEvent title="ขึ้นทะเบียนเป็นแหล่งมรดกโลก"
                                    subtitle="12 ธันวาคม พ.ศ. 2534"
                                    icon={<i className="material-icons md-18">textsms</i>
                                 }  titleStyle={{color: "#a5494d", fontSize:"16px"}} 
                                 contentStyle={{backgroundColor: "#ecd292", color: "#d27254"}}
                                 subtitleStyle={{color: "#e5a267", fontSize:"14px"}}
                                 bubbleStyle={{backgroundColor: "#e5a267"}}
                                 iconColor="#63223c"
                                 
                      >
                      <p className={classes.descCard}>
                      อุทยานประวัติศาสตร์สุโขทัยได้รับการประกาศคุ้มครองครั้งแรกตามประกาศราชกิจจานุเบกษา เล่มที่ 92 ตอนที่ 112 ลงวันที่ 2 สิงหาคม พ.ศ. 2504 
                          </p>

                          <div id="buttons" className={classes.buttonCard}>
                                    <Button href="landing-page-changlom" color="timelinebutton" size="sm"> ศึกษาเพิ่มเติม.. </Button>
                                  </div>

                      </TimelineEvent>
                        <div style={{position: 'relative', margin: '10px 0px', paddingLeft: '45px', textAlign: 'left'}}>
                        <img src="http://thailandscanme.com/images/upload/sti001/sti001-07-l.jpg" width= '95%' height ='100%' />
                        </div>

              </Timeline>,
      </div>
    );
  }
}

export default withStyles(productStyle)(ProductSection);
