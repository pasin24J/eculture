import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";

import productStyle from "assets/jss/material-kit-react/views/landingPageSections/productStyle.jsx";

class ProductSection extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={8}>
            <h5 className={classes.descCard}>
            อุทยานประวัติศาสตร์สุโขทัย มีโบราณสถานอยู่ทั้งหมด 35 แห่ง หนึ่งในนั้นคือวัดช้างล้อม วัดช้างล้อมเป็นกลุ่มโบราณสถานขนาดใหญ่ที่สุด ในเขตนอกกำแพงเมืองด้านทิศตระวันออก มีแนวคูน้ำล้อมรอบ กว้างประมาณ 100 เมตร ยาวประมาณ 157 เมตร มีเจดีย์ทรงกลมแบบลังกาองค์ใหญ่ก่อด้วยอิฐ ที่ฐานเจดีย์มีช้างปูนปั้นโผล่ครึ่งตัว จำนวน 32 เชือก มีระเบียงโดยรอบ (เชื่อว่าไว้สำหรับเวียนประทักษินเพื่อเป็นพุทธบูชา) มีวิหารด้านหน้าเจดีย์หลังใหญ่หันหน้าทางด้านทิศตระวันออก 
            </h5>
          </GridItem>
        </GridContainer>
        <div class="sketchfab-embed-wrapper"><iframe width="100%" height="auto"  src="https://sketchfab.com/models/74009add33404e1b8696d8066685116b/embed?autospin=0.2&amp;autostart=1&amp;preload=1" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
          <p style={{fontSize: '13px', fontWeight: 'normal', margin:' 5px', color:' #4A4A4A'}}>
              <a href="https://sketchfab.com/models/74009add33404e1b8696d8066685116b?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style={{fontWeight: 'bold', color: '#1CAAD9'}}>Wat Chang Lom</a>
              by <a href="https://sketchfab.com/sirichaik?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style={{fontWeight: 'bold', color: '#1CAAD9'}}>sirichaik</a>
              on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style={{fontWeight: 'bold', color: '#1CAAD9'}}>Sketchfab</a>
          </p>
        </div>

        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={8}>
            <h5 className={classes.descCard}>
            ภายนอกคูน้ำด้านทิศตระวันออกมีโบสถ์หนึ่งหลัง ได้พบศิลาจารึกที่วัดนี้ เรีกยว่า จารึกหลักที่ 106 จารึกวัดช้างล้อม กล่าวถึงเหตุการณ์ในช่วงต้นพุทธศตวรรษที่ 20 ว่าพนมไสดำ ผัวแม่นมเทศ ได้บวชตามพระมหาธรรมราชาลิไท ที่วัดป่ามะม่วง (บวชได้กี่วันไม่ปรากฏ) บวชอีกครั้งเมื่อพระมหาเทวีสิ้นพระชนน์ และบวชอีกครั้งอุทิศที่ดินของตนสร้างวัดเพื่ออุทิศบุญกุศลถวายแด่พระมหาธรรมราชาลิไท สร้างพระบทอุทิศบุญกุศลแด่พระมหาเทวี และถวายภรรยา บุตร เป็นผู้ดูแลพระพุทธศาสนา ในปีพุทธศักราช 1972
            </h5>
          </GridItem>
        </GridContainer>
        <div>
        <div style={{position: 'relative', margin: '10px 0px', textAlign: 'center'}}>
                <img src="https://www.silpa-mag.com/wp-content/uploads/2018/03/%E0%B8%AD%E0%B8%B8%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%99%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%A7%E0%B8%B1%E0%B8%95%E0%B8%B4%E0%B8%A8%E0%B8%B2%E0%B8%AA%E0%B8%95%E0%B8%A3%E0%B9%8C%E0%B8%AA%E0%B8%B8%E0%B9%82%E0%B8%82%E0%B8%97%E0%B8%B1%E0%B8%A2.jpg" width= '95%' height ='auto' />
                </div>
        </div>
        <br/>
        
      </div>
    );
  }
}

export default withStyles(productStyle)(ProductSection);
