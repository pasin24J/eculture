import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import InfoArea from "components/InfoArea/InfoArea.jsx";

import productStyle from "assets/jss/material-kit-react/views/landingPageSections/productStyle.jsx";

class ProductSection extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={8}>
            <h5 className={classes.descCard}>
            อาณาจักรอยุธยาเริ่มก่อตั้งขึ้นเมื่อปี พ.ศ. 1893 โดยเวลานั้นพระเจ้าอู่ทองได้ทำการสถาปนากรุงศรีอยุธยาขึ้นมา เนื่องจากกรุงสุโขทัยในเวลานั้นกำลังเริ่มเสื่อมอำนาจลง และหัวเมืองต่าง ๆ ก็เริ่มคิดแข็งข้อ พระเจ้าอู่ทอง จึงเริ่มสร้างกองกำลังและแต่งตั้งตนเองขึ้นเป็นผู้นำคนไทย ที่อาศัยอยู่บริเวณลุ่มแม่น้ำเจ้าพระยาตอนกลาง และตอนล่าง  เพื่อสถาปนากรุงศรีอยุธยาให้เป็นอิสระจากกรุงสุโขทัย และตั้งชื่อราชธานีใหม่ว่า “อโยธยา” 
            </h5>
          </GridItem>
        </GridContainer>

        <div>
        <div style={{position: 'relative', margin: '10px 0px', textAlign: 'center'}}>
                <img src="https://www.silpa-mag.com/wp-content/uploads/2018/03/%E0%B8%AD%E0%B8%B8%E0%B8%97%E0%B8%A2%E0%B8%B2%E0%B8%99%E0%B8%9B%E0%B8%A3%E0%B8%B0%E0%B8%A7%E0%B8%B1%E0%B8%95%E0%B8%B4%E0%B8%A8%E0%B8%B2%E0%B8%AA%E0%B8%95%E0%B8%A3%E0%B9%8C%E0%B8%AA%E0%B8%B8%E0%B9%82%E0%B8%82%E0%B8%97%E0%B8%B1%E0%B8%A2.jpg" width= '95%' height ='auto' />
                </div>
        </div>
      
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={8}>
            <h5 className={classes.descCard}>
            โดยราชธานีใหม่นี้ตั้งอยู่ในบริเวณหนองโสนหรือบึงพระราม  ซึ่งในปัจจุบันก็คือจังหวัดพระนครศรีอยุธยานั่นเอง  ส่วนพระเจ้าอู่ทองก็ทรงเป็นปฐมกษัตริย์องค์แรกในราชวงศ์อู่ทอง และทรงพระนาว่า  “สมเด็จพระรามาธิบดีที่ 1” ซึ่งพระองค์ได้ทรงขึ้นครองราชย์เพื่อปกครองกรุงศรีอยุธยาได้อย่างยาวอนานเป็นเวลาถึง  20  ปี 
            </h5>
          </GridItem>
        </GridContainer>

        <div class="sketchfab-embed-wrapper"><iframe  width="100%" height="auto"  src="https://sketchfab.com/models/d10d6fde9da8458189e7059fba742e4e/embed?autospin=0.2&amp;autostart=1&amp;preload=1" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
          <p style={{fontSize: '13px', fontWeight: 'normal', margin:' 5px', color:' #4A4A4A'}}>
              <a href="https://sketchfab.com/models/d10d6fde9da8458189e7059fba742e4e?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style={{fontWeight: 'bold', color: '#1CAAD9'}}>Wad Phra Sri Son Petch</a>
              by <a href="https://sketchfab.com/sirichaik?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style={{fontWeight: 'bold', color: '#1CAAD9'}}>sirichaik</a>
              on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campaign=share-popup" target="_blank" style={{fontWeight: 'bold', color: '#1CAAD9'}}>Sketchfab</a>
          </p>
        </div>

        <br/>
        
      </div>
    );
  }
}

export default withStyles(productStyle)(ProductSection);
