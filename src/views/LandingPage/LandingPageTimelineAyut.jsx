import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// @material-ui/icons

// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import Parallax from "components/Parallax/Parallax.jsx";

import landingPageStyle from "assets/jss/material-kit-react/views/landingPage.jsx";

// Sections for this page
import ProductSection from "./Sections/ProductSection";
import TimelineSection from "./Sections/TimelineSectionAyut";
import TeamSection from "./Sections/TeamSection";
import WorkSection from "./Sections/WorkSection";

const dashboardRoutes = [];

class LandingPage extends React.Component {
  render() {
    const { classes, ...rest } = this.props;
    return (
      <div>
        <Header
          color="transparent"
          routes={dashboardRoutes}
          brand="e-Culture Education"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 400,
            color: "white"
          }}
          {...rest}
        />
        <Parallax filter image={require("assets/img/suko.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12} sm={12} md={6}>
                <h1 className={classes.title}>กรุงศรีอยุธยา</h1>
                <h4>
                อาณาจักรอยุธยาเริ่มก่อตั้งขึ้นเมื่อปี พ.ศ. 1893 โดยเวลานั้นพระเจ้าอู่ทองได้ทำการสถาปนากรุงศรีอยุธยาขึ้นมา เนื่องจากกรุงสุโขทัยในเวลานั้นกำลังเริ่มเสื่อมอำนาจลง และหัวเมืองต่าง ๆ ก็เริ่มคิดแข็งข้อ พระเจ้าอู่ทอง จึงเริ่มสร้างกองกำลังและแต่งตั้งตนเองขึ้นเป็นผู้นำคนไทย 
                </h4>
                <br />
                <Button
                  color="danger"
                  size="lg"
                  href="https://www.youtube.com/watch?v=YRv0fwt6pFA"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fas fa-play" />Watch video
                </Button>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div className={classes.container}>
            <TimelineSection />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(landingPageStyle)(LandingPage);
