import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// @material-ui/icons
import Camera from "@material-ui/icons/Camera";
import Palette from "@material-ui/icons/Palette";
import Favorite from "@material-ui/icons/Favorite";
// core components
import Header from "components/Header/Header.jsx";
import Footer from "components/Footer/Footer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import HeaderLinks from "components/Header/HeaderLinks.jsx";
import NavPills from "components/NavPills/NavPills.jsx";
import Parallax from "components/Parallax/Parallaxgall.jsx";

import profile from "assets/img/faces/christian.jpg";

import n1 from "assets/img/examples/n1.jpg";
import n2 from "assets/img/examples/n2.jpg";
import n3 from "assets/img/examples/n3.jpg";
import n4 from "assets/img/examples/n4.jpg";
import n5 from "assets/img/examples/n5.jpg";
import s1 from "assets/img/examples/s1.jpg";
import s2 from "assets/img/examples/s2.jpg";
import s3 from "assets/img/examples/s3.jpg";
import s4 from "assets/img/examples/s5.jpg";

import s5 from "assets/img/examples/s4.jpg";
import e1 from "assets/img/examples/e1.jpg";
import e2 from "assets/img/examples/e2.jpg";
import e3 from "assets/img/examples/e3.jpg";
import e4 from "assets/img/examples/e4.jpg";
import e5 from "assets/img/examples/e5.jpg";
import c1 from "assets/img/examples/c1.jpg";
import c2 from "assets/img/examples/c2.jpg";
import c3 from "assets/img/examples/c3.jpg";
import c4 from "assets/img/examples/c4.jpg";
import c5 from "assets/img/examples/c5.jpg";


import profilePageStyle from "assets/jss/material-kit-react/views/profilePage.jsx";

class ProfilePage extends React.Component {
  render() {
    const { classes, ...rest } = this.props;
    const imageClasses = classNames(
      classes.imgRaised,
      classes.imgRoundedCircle,
      classes.imgFluid
    );
    const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);
    return (
      <div>
        <Header
          color="transparent"
          brand="e-Culter Education"
          rightLinks={<HeaderLinks />}
          fixed
          changeColorOnScroll={{
            height: 200,
            color: "white"
          }}
          {...rest}
        />
        <Parallax image={require("assets/img/gall.jpg")}>
          <div className={classes.container}>
            <GridContainer>
              <GridItem>
                <div className={classes.brand}>
                  <h1 className={classes.title}>แกลลอรี่</h1>
                </div>
              </GridItem>
            </GridContainer>
          </div>
        </Parallax>
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              
              <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={8} className={classes.navWrapper}>
                  <NavPills
                    alignCenter
                    color="primary"
                    tabs={[
                        {
                        tabButton: "ภาคเหนือ",
                        tabContent: (
                          <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={4}>
                              <img
                                alt="..."
                                src={n1}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={n2}
                                className={navImageClasses}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <img
                                alt="..."
                                src={n5}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={n4}
                                className={navImageClasses}
                              />
                            </GridItem>
                          </GridContainer>
                        )
                      },
                      {
                        tabButton: "ภาคกลาง",
                        tabContent: (
                          <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={4}>
                              <img
                                alt="..."
                                src={c1}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={c2}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={c3}
                                className={navImageClasses}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <img
                                alt="..."
                                src={c4}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={c5}
                                className={navImageClasses}
                              />
                            </GridItem>
                          </GridContainer>
                        )
                      },
                      {
                        tabButton: "ภาคใต้",
                        tabContent: (
                          <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={4}>
                              <img
                                alt="..."
                                src={s4}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={s3}
                                className={navImageClasses}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <img
                                alt="..."
                                src={s2}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={s1}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={s5}
                                className={navImageClasses}
                              />
                            </GridItem>
                          </GridContainer>
                        )
                      },
                      {
                        tabButton: "ภาคตะวันออก",
                        tabContent: (
                          <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={4}>
                              <img
                                alt="..."
                                src={e4}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={e3}
                                className={navImageClasses}
                              />
                            </GridItem>
                            <GridItem xs={12} sm={12} md={4}>
                              <img
                                alt="..."
                                src={e2}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={e1}
                                className={navImageClasses}
                              />
                              <img
                                alt="..."
                                src={e5}
                                className={navImageClasses}
                              />
                            </GridItem>
                          </GridContainer>
                        )
                      }
                    ]}
                  />
                </GridItem>
              </GridContainer>

              <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={6}>
                  <div className={classes.profile}>
                    <div className={classes.name}>
                      <Button justIcon link className={classes.margin5}>
                        <i className={"fab fa-twitter"} />
                      </Button>
                      <Button justIcon link className={classes.margin5}>
                        <i className={"fab fa-instagram"} />
                      </Button>
                      <Button justIcon link className={classes.margin5}>
                        <i className={"fab fa-facebook"} />
                      </Button>
                    </div>
                  </div>
                </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(profilePageStyle)(ProfilePage);
