import { Database } from '../configs/firebase'

export const getAllCustomers = async () => {
    try {
        let docs = await Database.collection('results').where('id', '==', "MOC-97608").get()
        let data = []
        docs.forEach(doc => data.push({ uid: doc.id, ...doc.data() }))
        return data
    } catch (err) {
        console.error('Error:', err.message)
    }
}