import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

var config = {
    apiKey: "AIzaSyCnpcH3QRj99HueemMXaZeavfCWqRvxQNU",
    authDomain: "eculture-83b2b.firebaseapp.com",
    databaseURL: "https://eculture-83b2b.firebaseio.com",
    projectId: "eculture-83b2b",
    storageBucket: "eculture-83b2b.appspot.com",
    messagingSenderId: "317218025383"
  };

firebase.initializeApp(config)

const firestore = firebase.firestore()
const settings = {
    timestampsInSnapshots: true
}
firestore.settings(settings)

export const Authentication = firebase.auth();
export const Database = firestore;
export const Storage = firebase.storage();
export const GAPI_KEY = "AIzaSyDvkwTjdfMHNOd2WGNRhfR1G1BQV5YiJbQ";
