import Components from "views/Components/Components.jsx";
import LandingPage from "views/LandingPage/LandingPage.jsx";
import LandingPageTimeline from "views/LandingPage/LandingPageTimeline.jsx";
import ProfilePage from "views/ProfilePage/ProfilePage.jsx";
import LoginPage from "views/LoginPage/LoginPage.jsx";
import SearchPage from "views/SearchPage/SearchPage.jsx";
import LandingPageTimelineAyut from "../views/LandingPage/LandingPageTimelineAyut";
import LandingPagechanglom from "../views/LandingPage/LandingPagechanglom";
import LandingPagesrisunpet from "../views/LandingPage/LandingPagesrisunpet";
import Activity from "views/LandingPage/Activity.jsx";


var indexRoutes = [
  { path: "/landing-page", name: "LandingPage", component: LandingPage },
  { path: "/landing-page-changlom", name: "LandingPagechanglom", component: LandingPagechanglom },
  { path: "/landing-page-srisunpet", name: "LandingPagesrisunpet", component: LandingPagesrisunpet },
  { path: "/landing-page-timeline", name: "LandingPageTimeline", component: LandingPageTimeline },
  { path: "/landing-page-timeline-ayut", name: "LandingPageTimelineAyut", component: LandingPageTimelineAyut },
  { path: "/profile-page", name: "ProfilePage", component: ProfilePage },
  { path: "/login-page", name: "LoginPage", component: LoginPage },
  { path: "/search-page", name: "SearchPage", component: SearchPage },
  { path: "/activity", name: "ActPage", component: Activity },
  { path: "/", name: "Components", component: Components }
];

export default indexRoutes;
