import { title } from "assets/jss/material-kit-react.jsx";

const productStyle = {
  section: {
    padding: "10px 0",
    textAlign: "center"
  },
  title: {
    ...title,
    marginBottom: "1rem",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  description: {
    color: "#999"
  },
  descCard: {
    fontSize: "13px",
    paddingLeft: "3px",
    paddingRight: "3px",
    marginBottom: "10px",
    fontFamily: 'Pridi',
    height: "auto",
    overflowY: "hidden",
    textOverflow: 'ellipsis',
    color:'#767676'
  },
  
};

export default productStyle;
