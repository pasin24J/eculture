import { title } from "assets/jss/material-kit-react.jsx";

const timelineStyle = {
  section: {
    textAlign: "center",
    // color: "red"
  },
  title: {
    ...title,
    marginBottom: "1rem",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none",
    color: "#e5a267"
  },
  description: {
    color: "#999"
  },
  descCard: {
    fontSize: "13px",
    paddingLeft: "3px",
    paddingRight: "3px",
    marginBottom: "10px",
    fontFamily: 'Pridi',
    height: "auto",
    overflowY: "hidden",
    textOverflow: 'ellipsis'
  },
  buttonCard: {
    display: "flex",
    justifyContent: "flex-end",
    paddingRight: "10px"
  },
};

export default timelineStyle;
