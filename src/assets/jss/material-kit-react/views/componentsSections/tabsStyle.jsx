import { container } from "assets/jss/material-kit-react.jsx";

const tabsStyle = {
  section: {
    background: "#EEEEEE",
    padding: "5px 0"
  },
  title: {
    marginTop: "5px 0"
  },
  descCard: {
    fontSize: "13px",
    paddingLeft: "3px",
    paddingRight: "3px",
    marginBottom: "10px",
    fontFamily: 'Pridi',
    height: "auto",
    overflowY: "hidden",
    textOverflow: 'ellipsis'
  },
  container,
  textCenter: {
    textAlign: "center"
  }
};

export default tabsStyle;
