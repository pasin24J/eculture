import { container, title } from "assets/jss/material-kit-react.jsx";
import customCheckboxRadioSwitch from "assets/jss/material-kit-react/customCheckboxRadioSwitch.jsx";

const basicsStyle = {
  sections: {
    padding: "15px 0"
  },
  container,
  title: {
    ...title,
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  space50: {
    height: "50px",
    display: "block"
  },
  space70: {
    height: "70px",
    display: "block"
  },
  icons: {
    width: "17px",
    height: "17px",
    color: "#FFFFFF"
  },
  imgCard: {
    width: "100%",
    maxHeight: "250px"
  },
  imgRounded: {
    borderRadius: "6px !important"
  },
  imgRaised: {
    boxShadow:
      "-5px 5px 15px -8px rgba(0, 0, 0, 0.24), 5px 8px 10px -5px rgba(0, 0, 0, 0.2)"
  },
  descCard: {
    fontSize: "16px",
    paddingLeft: "20px",
    paddingRight: "10px",
    marginBottom: "10px",
    fontFamily: 'Pridi',
    height: "90px",
    overflowY: "hidden",
    textOverflow: 'ellipsis'
  },
  buttonCard: {
    display: "flex",
    justifyContent: "flex-end",
    paddingRight: "10px",
    paddingBottom: "10px"
  },
  topicCard: {
    paddingLeft: "10px"
  },
  ...customCheckboxRadioSwitch
};

export default basicsStyle;
